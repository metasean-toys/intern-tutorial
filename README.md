### Branches

- **master** - baseline copy of the tutorial developed from https://github.com/theintern/intern-tutorial


### Notes

- To run **unit tests**:   
    `./node_modules/.bin/intern-client config=tests/intern`

- To run **functional tests**:   
    `./node_modules/.bin/intern-runner config=tests/intern`
    > NOTE: Set-up with a temporary BrowserStack account.  